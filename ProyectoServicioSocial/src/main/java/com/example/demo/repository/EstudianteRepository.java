package com.example.demo.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.clases.Estudiante;

public interface EstudianteRepository extends JpaRepository<Estudiante,Integer> {
	
}
