package com.example.demo.scv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import com.example.demo.clases.Estudiante;

public class CSVUtils {

	public static void CSVestudiantes(PrintWriter writer, List<Estudiante> estudiantes) throws IOException {
		
		try (
				
				CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("id","matricula","ap_paterno","ap_materno", "nombre","fecha_registro","correo"));
		) {
			for (Estudiante estudiante : estudiantes) {
				List<String> data = Arrays.asList(
						String.valueOf(estudiante.getId()),
						estudiante.getMatricula(),
						estudiante.getAp_paterno(),
						estudiante.getAp_materno(),
						estudiante.getNombre(),
						estudiante.getFecha_registro(),
						estudiante.getCorreo()
					);
				
				csvPrinter.printRecord(data);
			}
			csvPrinter.flush();
		} catch (Exception e) {
			System.out.println("Reading CSV Error!");
			e.printStackTrace();
		} finally {
		}
	}
	
	public static List<Estudiante> analizar_ArchivoCSV(InputStream is) {
		BufferedReader fileReader = null;
		CSVParser csvParser = null;

		List<Estudiante> estudiantes = new ArrayList<Estudiante>();
		
		try {
			fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			csvParser = new CSVParser(fileReader,
					CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
 
			Iterable<CSVRecord> csvRecords = csvParser.getRecords();
			
			for (CSVRecord csvRecord : csvRecords) {
				Estudiante estudiante = new Estudiante(
						Long.parseLong(csvRecord.get("id")),
						csvRecord.get("matricula"),
						csvRecord.get("ap_paterno"),
						csvRecord.get("ap_materno"),
						csvRecord.get("nombre"),
						csvRecord.get("fecha_registro"),
						csvRecord.get("correo")
						);
				
				estudiantes .add(estudiante);
			}
			
		} catch (Exception e) {
			System.out.println("Reading CSV Error!");
			e.printStackTrace();
		} finally {
			try {
				fileReader.close();
				csvParser.close();
			} catch (IOException e) {
				System.out.println("Closing fileReader/csvParser Error!");
				e.printStackTrace();
			}
		}
		
		return estudiantes;
	}
}