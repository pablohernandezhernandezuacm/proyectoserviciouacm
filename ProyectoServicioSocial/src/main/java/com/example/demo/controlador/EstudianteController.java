package com.example.demo.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import org.springframework.web.servlet.ModelAndView;


import com.example.demo.clases.Estudiante;
import com.example.demo.service.ArchivoService;
import com.example.demo.service.EstudianteService;

@Controller
public class EstudianteController {

@Autowired
 EstudianteService service;

@Autowired
ArchivoService archivoService;



@GetMapping("/listadeestudiantes")
public String VistaPaginaPrincipal(Model model) {
List<Estudiante>listadeestudiantes= service.listAll();
model.addAttribute("listadeestudiantes", listadeestudiantes.addAll(listadeestudiantes));

	return "index";
}

@RequestMapping("/new")
public String nuevoEstudiante(Model model) {
	Estudiante estudiante = new Estudiante();
	model.addAttribute("estudiante", estudiante);
	return "guardarinformacion";
}


@RequestMapping(value="/upload", method = RequestMethod.POST)
public String uploadMultipartFile(@RequestParam("uploadfile") MultipartFile file, Model model) {
	try {
		archivoService.store(file);
		model.addAttribute("message", "File uploaded successfully!");
	} catch (Exception e) {
		model.addAttribute("message", "Fail! -> uploaded filename: " + file.getOriginalFilename());
	}
    return "guardarinformacion";
}

@RequestMapping(value= "/file", method = RequestMethod.GET)
public void downloadFile(HttpServletResponse response) throws IOException{
	response.setContentType("text/csv");
	response.setHeader("Content-Disposition", "attachment; filename=lista_estudiantes.csv");
	
	archivoService.loadFile(response.getWriter());	
}



@RequestMapping(value = "/save", method = RequestMethod.POST)
public String saveEstudiante(@ModelAttribute("estudiante") Estudiante estudiante, long matricula,Model model) {
	service.save(estudiante);
    return "redirect:/";
}


@RequestMapping("/edit/{id}")
public ModelAndView showEditProductPage(@PathVariable(name = "id") int id, Model model) {
    ModelAndView mav = new ModelAndView("guardar_estudiante");
    Estudiante estudiante = service.get(id);
    mav.addObject("estudiante", estudiante);

    return mav;
}

@RequestMapping("/delete/{id}")
public String deleteEstudiante(@PathVariable(name = "id") int id) {
	service.delete(id);
    return "redirect:/";       
}


}
