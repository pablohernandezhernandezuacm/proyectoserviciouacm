package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.clases.Estudiante;
import com.example.demo.repository.EstudianteRepository;

@Service
@Transactional
public class EstudianteService {
	
@Autowired
public EstudianteRepository service;

public List<Estudiante>listAll(){
	return service.findAll();
}

public void save(Estudiante estudiante) {
service.save(estudiante);	
}

public  Estudiante get(int  id) {
	return service.findById(id).get();
}

public void delete(int  id) {
service.deleteById(id);
}

}
