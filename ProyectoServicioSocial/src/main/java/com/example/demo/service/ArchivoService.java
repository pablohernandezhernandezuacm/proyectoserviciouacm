package com.example.demo.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.clases.Estudiante;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.scv.CSVUtils;

@Service
public class ArchivoService {

@Autowired
EstudianteRepository estudianteRepository;

public void store(MultipartFile file){
	try {
		List<Estudiante> listaEstudiantes = CSVUtils.analizar_ArchivoCSV(file.getInputStream());
		// Guardar la informacion en la base de datos
		estudianteRepository.saveAll(listaEstudiantes);
    } catch (IOException e) {
    	throw new RuntimeException("FAIL! -> message = " + e.getMessage());
    }
}

// Cargar la información en excel
public void loadFile(PrintWriter writer) {
	List<Estudiante> estudiantes = (List<Estudiante>) estudianteRepository.findAll();
	
	try {
		CSVUtils.CSVestudiantes(writer, estudiantes);
	} catch (IOException e) {}
}
}