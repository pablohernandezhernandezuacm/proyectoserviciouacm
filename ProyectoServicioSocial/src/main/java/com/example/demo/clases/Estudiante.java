package com.example.demo.clases;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name="estudiante")
@Data
public class Estudiante {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name="matricula")
	private String matricula;
	@Column(name="ap_paterno")
	private String ap_paterno;
	@Column(name="ap_materno")
	private String ap_materno;
	@Column(name="nombre")
	private String nombre;
	@Column(name="fecha_registro")
	private String fecha_registro;
	@Column(name="correo")
	private String correo;
	
	public Estudiante (Long id,String matricula,String ap_paterno,String ap_materno, String nombre, String fecha_registro, String correo) {
		this.id=id;
		this.matricula=matricula;
		this.ap_paterno=ap_paterno;
		this.ap_materno=ap_materno;
		this.nombre=nombre;
		this.fecha_registro=fecha_registro;
		this.correo=correo;
	}

	public Estudiante() {
		
	}
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAp_paterno() {
		return ap_paterno;
	}

	public void setAp_paterno(String ap_paterno) {
		this.ap_paterno = ap_paterno;
	}

	public String getAp_materno() {
		return ap_materno;
	}

	public void setAp_materno(String ap_materno) {
		this.ap_materno = ap_materno;
	}

	public String getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(String fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	@Override
	public String toString() {
		return "Estudiante [id=" + id + " matricula=" + matricula + ", nombre=" + nombre + ", ap_paterno=" + ap_paterno + ",ap_materno="+ap_materno+",fecha_registro="+fecha_registro+",correo="+correo+"]";
	}
	
}
